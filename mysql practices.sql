CREATE TABLE Persons (
    PersonID int,
    LastName varchar(255),
    FirstName varchar(255),
    Address varchar(255),
    City varchar(255)
);
insert into Persons (PersonID ,
    LastName ,
    FirstName ,
    Address ,
    City) values (1,'pandi','arun','tnagar','chennai');
    
    insert into Persons (PersonID ,
    LastName ,
    FirstName ,
    Address ,
    City) values (2,'kaala','guna','tnagar','chennai');
    
    insert into Persons (PersonID ,
    LastName ,
    FirstName ,
    Address ,
    City) values (3,'raja','raja','tnagar','chennai');
    
    insert into Persons (PersonID ,
    LastName ,
    FirstName ,
    Address ,
    City) values (4,'kumar','ajith','ecr','chennai');
    
    insert into Persons (PersonID ,
    LastName ,
    FirstName ,
    Address ,
    City) values (5,'suriya','karthi','mambalam','chennai');
    
    select * from Persons;
    
    select LastName from Persons;
    select FirstName from Persons;
    select * from Persons;
    select LastName from Persons where Address='tnagar';
    select * from Persons where Address='tnagar';

    select * from Persons order by Address;
select * from Persons order by FirstName;
select * from Persons ;
select * from Persons order by PersonID;
select * from Persons order by PersonID desc;
select distinct Address from Persons;
ALTER TABLE Persons ADD Email varchar(255);    
insert into Persons values (6,'kumar','ajith','ecr','chennai','arun25ya@gmail.com');
select * from Persons;
update Persons set Email='ap@gmail.com'; 
update Persons set Email='ap1@gmail.com' where PersonID=1; 


update Persons set Email='ap2@gmail.com' where PersonID=6;
delete Email from Persons;
alter table Persons  add ddd date;
update Persons set ddd='2023-04-30'; 

delete from Persons where PersonID=2;

update Persons set Email='ap1@gmail.com' where PersonID=6; 
select * from Persons where Address='tnagar' and City='chennai';
select * from Persons where Address='tnagar' or Email='ap1@gmail.com';
select * from Persons where not (Address='tnagar' or Email='ap1@gmail.com');
select * from Persons where Address in('tnagar','ecr');

alter table Persons add date_order date;
alter table Persons drop date_order;
select * from Persons;
update Persons set ddd='2023-05-01' where PersonId in (1,4,6);

alter table Persons add amoutn int; 
update Persons set amoutn=25000 where PersonId in (1,4,6);
alter table Persons rename column amoutn to amount;
select * from Persons;
update Persons set amount=30000 where PersonId in (3,5);
select sum(amount) from Persons group by ddd;
select max(amount) from Persons group by ddd;
update Persons set amount=28000 where PersonId=5;
update Persons set amount=30000 where PersonId=4;
select max(amount) from Persons group by ddd;
select min(amount) from Persons group by ddd;
select avg(amount) from Persons group by ddd;
select count(amount) from Persons group by ddd;
set autocommit=off;
commit;
alter table Persons drop City;
select * from Persons;
rollback;
alter table Persons add City varchar(10);
update Persons set City='chennai';
select LastName,FirstName from Persons;
set autocommit=off;
commit;
delete from Persons where personId=6;
commit;
alter table Persons drop City;
alter table Persons add City varchar(29);
update persons set City='Chennai';

select * from persons ;
select sum(amount) from persons;
CREATE TABLE Persons2 (
    PersonID int,
    LastName varchar(255),
    FirstName varchar(255),
    Address varchar(255),
    City varchar(255)
);
insert into persons2 values(1,'dhoni','','ranchi','jarkhand');
insert into persons2 values
(2,'kohli','virat','delhi','delhi'),
(3,'kumar','surya','mumbai','mumbai'),
(4,'kumar','surya','mumbai','mumbai'),
(5,'kumar','surya','mumbai','mumbai'),
(6,'kumar','surya','mumbai','mumbai'),
(7,'kumar','surya','mumbai','mumbai'),
(8,'kumar','surya','mumbai','mummbai');
select * from persons2;
select * from persons2 join persons on persons.PersonID=persons2.PersonID;
select * from persons2 inner join persons on persons.PersonID=persons2.PersonID;
select * from persons2 left outer join persons on persons.PersonID=persons2.PersonID;
insert into persons values(7,'arun','','tvm','ap3@gmail.com',40000,'tvm');
select * from persons2 right outer join persons on persons.PersonID=persons2.PersonID;
update persons set PersonId=20 where PersonID=8;
select * from persons where amount between 25000  and 28000;
select * from persons having amount <26000;
select * from persons;
select * from persons inner join persons2 where persons.PersonID=persons2.PersonID;
show tables;
select * from persons;
select * from persons;
set autocommit=on;
insert into persons2 values(1,'dhoni','','ranchi','jarkhand');
insert into persons2 values
(2,'kohli','virat','delhi','delhi'),
(3,'kumar','surya','mumbai','mumbai'),
(4,'kumar','surya','mumbai','mumbai'),
(5,'kumar','surya','mumbai','mumbai'),
(6,'kumar','surya','mumbai','mumbai'),
(7,'kumar','surya','mumbai','mumbai'),
(8,'kumar','surya','mumbai','mummbai');
select * from persons2;
select * from persons2  join persons on persons.PersonID=persons2.PersonID;
SELECT persons.LastName AS A, persons2.LastName AS B, persons.City
FROM persons,persons2
WHERE  persons.PersonID= persons2.PersonID;
SELECT FirstName FROM persons
UNION
SELECT LastName FROM persons2;
create table e1(ename varchar(10),deptno int);
create table e2(dname varchar(10),deptno int);
insert into e1 value
('a',10),
('b',20),
('c',30),
('d',20);
insert into e2 value
('d1',20),
('d2',10),
('d3',30);
select * from e1,e2;
select * from persons,persons2;
create view v1 as select PersonID,FirstName,City from persons;
select * from  v1;
drop view v1;
rollback;

SELECT FirstName FROM persons
UNION all
SELECT LastName FROM persons2;
SELECT FirstName FROM persons
UNION
SELECT LastName FROM persons2;
select * from persons where lastName like '%a';
select * from persons;
select * from persons limit 1,2;
set autocommit=off;
commit;
delete from persons limit 1;
rollback;
insert into Persons values (1,'arun','padi','chennai','arun25ya@gmail.com','50000','tnagar'),
(2,'guna','tnagar','chennai','kga@gmail.com','200','kaala'),
(1,'periya','sam','chennai','sp.com','2000','velachery'),
(1,'go','kul','chennai','g@gmail.com','20000','tnagar');
update persons set firstname='pandi' where personid=1;
select * from persons;
update persons set personid=3 where city='velachery';
update persons set personid=4 where LastName='go';
alter table persons modify amount int default 2;
update persons set amount=100;
desc persons;
commit;
select * from v1;
delete from v1 where Personid=6;
rollback;
create table csk(player varchar(20) not null,
position  varchar(20) not null,
jerseyno numeric(20) not null,
country varchar(20) not null,
age int not null,
constraint pk_pl primary key(player),
constraint u_po unique(position),
constraint c_a check(age>18));
select * from csk;
update csk set age=19;
insert into csk values('dhawan',null,9,'india',25);
commit;
truncate table csk;
rollback;
drop table csk;
create table csk(player varchar(20) not null primary key,
position  varchar(20) not null unique ,
jerseyno numeric(20) not null ,
personid int not null ,
age int not null check(age>18),
 foreign key (personid) References persons(personid) );

alter table persons add constraint primary key (personid);

select * from csk;
select * from persons;
insert into csk values('dhoni','7',7,1,37);
rollback;
commit;
delete from csk where personid=1;
set autocommit=off;
select * from persons join  persons2 on persons.personId=persons2.personId;

alter table persons drop column amount;
select * from persons;
alter table persons rename column cities to City;
Delimiter $$
create procedure view()
begin
select * from persons;
end $$
delimiter ;

call view();
show grants;

delimiter //
create procedure cnt()
begin
declare total int default 0;
select count(personid) into total from persons;
select total;
end //
delimiter ;
delimiter //
create procedure cnt(
in cy varchar(10)
)
begin
declare total int default 0;
select count(personid) into total from persons where city=cy;
select total;
end //
delimiter ;
delimiter //
create procedure cnt(
in cy varchar(10),
out total int
)
begin
select count(personid) into total from persons where city=cy;
end //
delimiter ;
set @total=10;
call cnt('chennai',@total);
select @total;
call cnt('chennai');
select * from persons;
drop procedure cnt;

delimiter $$
create procedure inc(
inout count int,
in input int
)
begin

set count = count+input;
end $$
delimiter ;

set @count=5;
call inc(@count,9);
drop procedure inc;
select @count;
select * from persons;
delimiter $$
create procedure find(
in id int)
begin 
if id=1 or 4 then 
select * from persons where Personid=id;
elseif id =2 then
select * from persons where personid=id;
elseif id=3 then
select * from persons where personid=id;
else
select * from persons where personid=id;
end if;
end $$
delimiter ;

call find(6);

drop procedure find;
delimiter $$
create procedure find(
in id int)
begin 
  case id
      when 1 then 
           select * from persons where Personid=id;
      when 2 then
           select * from persons where personid=id;
      when 3 then
           select * from persons where personid=id;
      when 6 then
           select * from persons where personid=id;
end case;
end $$
delimiter ;

call find(1);
drop procedure find;



delimiter $$
create procedure loopdemo()
begin
  declare i int;
  set i=1;
 label:loop 
  if i>10 then
    leave label;
       select i;
	   set i=i+1;
   end if;    
end loop;
end $$
delimiter ;

call loopdemo();

drop procedure loopdemo;

delimiter $$
create function fulladdress(id int)
returns varchar(50)
deterministic
begin
   declare fulladd varchar(50);
   select concat(city," ",address) into fulladd from persons  where personid=id;
   return fulladd;
end$$
delimiter ;
drop function if exists fulladdress;
select FirstName,  fulladdress(PersonID) as fulladdress from persons;
select * from persons;
show function status;




create table persons_aut(
id int,
lastname varchar(50),
firstName varchar(50),
address varchar(50),
cities varchar(50),
action varchar (20),
changedate date);

delimiter $$
create trigger perchange after update on persons
for each row 
begin
insert into persons_aut set
id=old.personid,
lastname=old.lastname,
firstname=old.firstname,
address=old.address,
cities=old.city,
action='update',
changedate=now();
end $$
delimiter ;
delimiter $$
create trigger perchange after update on persons
for each row 
begin
insert into persons_aut set
id=new.personid,
lastname=new.lastname,
firstname=new.firstname,
address=new.address,
cities=new.city,
action='update',
changedate=now();
end $$
delimiter ;
select * from persons_aut;
update persons set lastname='surya' where personid=6;
drop trigger perchange;








